﻿using System.Data.OleDb;
using Npgsql;

namespace AccessToPostgreSQL
{
    public class DbConnectionFactory
    {
        private readonly string _postgresConnectionString;
        private readonly string _accessConnectionStringPrefix;

        public DbConnectionFactory(string postgresConnectionString, string accessConnectionStringPrefix)
        {
            _postgresConnectionString = postgresConnectionString;
            _accessConnectionStringPrefix = accessConnectionStringPrefix;
        }

        public NpgsqlConnection GetNpgsqlConnection()
        {
            return new NpgsqlConnection(_postgresConnectionString);
        }

        public OleDbConnection GetOleDbConnection(string accessDataSource)
        {
            return new OleDbConnection(_accessConnectionStringPrefix + accessDataSource);
        }
    }
}