﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using AccessToPostgreSQL.ColumnHelper;

namespace AccessToPostgreSQL.Presenter
{
    public class DataMigrationPresenter
    {
        private readonly IDataMigration _dataMigration;
        private readonly IDbService _dbService;
        private readonly ITableCreationQueryProvider _tableCreationQueryProvider;
        private DataModel _dataModel;
        private readonly IColumnInfoProviderFactory _columnInfoProviderFactory;
        private BackgroundWorker _backgroundWorker;

        public DataMigrationPresenter(IDataMigration dataMigration, IDbService dbService,
            ITableCreationQueryProvider tableCreationQueryProvider)
        {
            _dataMigration = dataMigration;
            _dbService = dbService;
            _tableCreationQueryProvider = tableCreationQueryProvider;
            _columnInfoProviderFactory = _tableCreationQueryProvider.ColumnInfoProviderFactory;
            _dataMigration.MigrationButtonClicked += MigrationButtonClicked;
            _dbService.InsertionCountChanged += InsertionCountChanged;
            InitializeBackgroundWorker();
            CreateDataModel();
        }

        private void InitializeBackgroundWorker()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += DoWork;
            _backgroundWorker.RunWorkerCompleted += RunWorkerCompleted;
        }

        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ShowMessage("Data migration has been completed.");
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                CreateSchemaAndTables();
                InsertTables();
                DoMigration();
            }
            catch (Exception exception)
            {
                NLogger.Trace(exception);
                ShowMessage("An error occurred during migration.");
            }
        }

        private void InsertionCountChanged(int insertedRowCount, string tableName)
        {
            UpdateMessage($"{insertedRowCount} rows have been inserted to table {tableName}");
        }

        private void CreateDataModel()
        {
            var serializer = new XmlSerializer(typeof(DataModel));
            var reader = XmlReader.Create("DataModel.xml");
            _dataModel = (DataModel)serializer.Deserialize(reader);
            reader.Close();
        }

        private void MigrationButtonClicked()
        {
            _backgroundWorker.RunWorkerAsync();
        }

        private void DoMigration()
        {
            UpdateMessage("Migration is in progress.");
            _dbService.DoMigration(_dataModel.StoreProcedures.Last().Name);
        }

        private void InsertTables()
        {
            foreach (var table in _dataModel.Tables)
            {
                var columnTypes = table.Columns.Select(c => _columnInfoProviderFactory.GetProvider(c.DataType.ToLower()).GetNpgsqlDbType(c)).ToList();
                _dbService.InsertIntoPostgresTable(_dataMigration.AccessDataSource, table.Name, columnTypes);
            }
        }

        private void CreateSchemaAndTables()
        {
            UpdateMessage("Migration has been started.");
            _dbService.CreateSchema();
            CreateTables();
            CreateStoreProcedures();
        }

        private void CreateStoreProcedures()
        {
            foreach (var storeProcedure in _dataModel.StoreProcedures)
            {
                _dbService.ExecuteNonQuery(storeProcedure.Query);
            }
        }

        private void CreateTables()
        {
            foreach (var table in _dataModel.Tables)
            {
                _dbService.ExecuteNonQuery(_tableCreationQueryProvider.GetQuery(table));
            }
        }

        private void ShowMessage(string completeMessage)
        {
            UpdateMessage(completeMessage);
            MessageBox.Show(completeMessage);
        }

        private void UpdateMessage(string completeMessage)
        {
            NLogger.Info(completeMessage);
            _dataMigration.UpdateToolTip(completeMessage);
        }

        public void Show()
        {
            _dataMigration.Show();
        }
    }

    public interface IDataMigration
    {
        void Show();
        event Action MigrationButtonClicked;
        string AccessDataSource { get; }
        void UpdateToolTip(string toolTip);
    }
}
