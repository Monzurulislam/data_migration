﻿using System.Collections.Generic;
using NpgsqlTypes;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class CommonColumnTypeProvider : ColumnTypeProvider
    {
        private readonly Dictionary<string, string> _typeMapperDictionary;
        private readonly Dictionary<string, NpgsqlDbType> _npgsqlDbTypes;

        public CommonColumnTypeProvider()
        {
            _typeMapperDictionary = new Dictionary<string, string>
            {
                {"double", "double precision"},
                {"integer", "integer"},
                {"serial", "serial"},
                {"text", "text"},
                {"date", "timestamp"},
                {"timestamp", "timestamp"},
                {"boolean", "boolean"},
                {"smallint", "smallint"},
                {"bigint", "bigint"},
                {"int", "int"},
                {"uuid", "uuid"},
                {"single", "real"}
            };

         _npgsqlDbTypes = new Dictionary<string, NpgsqlDbType>()
        {
            {"double", NpgsqlDbType.Double },
            {"integer", NpgsqlDbType.Integer},
            {"serial", NpgsqlDbType.Integer},
            {"text", NpgsqlDbType.Text},
            {"date", NpgsqlDbType.Timestamp},
            {"timestamp", NpgsqlDbType.Timestamp},
            {"boolean", NpgsqlDbType.Boolean},
            {"smallint", NpgsqlDbType.Smallint},
            {"bigint", NpgsqlDbType.Bigint},
            {"int", NpgsqlDbType.Integer},
            {"uuid", NpgsqlDbType.Uuid },
            {"single", NpgsqlDbType.Real }
        };
    }

        public override NpgsqlDbType GetNpgsqlDbType(Column column)
        {
            return _npgsqlDbTypes[column.DataType.ToLower()];
        }

        public override string TypeName { get; }

        protected override string GetColumnTypeInfo(Column column)
        {
            return _typeMapperDictionary[column.DataType.ToLower()];
        }
    }
}