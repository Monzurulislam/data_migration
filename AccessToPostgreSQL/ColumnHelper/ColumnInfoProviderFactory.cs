﻿using System.Collections.Generic;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class ColumnInfoProviderFactory : IColumnInfoProviderFactory
    {
        private readonly Dictionary<string, ColumnTypeProvider> _columnInfoDictionary;

        public ColumnInfoProviderFactory()
        {
            _columnInfoDictionary = new Dictionary<string, ColumnTypeProvider>()
            {
                {"integer", new CommonColumnTypeProvider()},
                {"serial", new CommonColumnTypeProvider()},
                {"text", new CommonColumnTypeProvider()},
                {"date", new CommonColumnTypeProvider()},
                {"timestamp", new CommonColumnTypeProvider()},
                {"boolean", new CommonColumnTypeProvider()},
                {"smallint", new CommonColumnTypeProvider()},
                {"bigint", new CommonColumnTypeProvider()},
                {"int", new CommonColumnTypeProvider()},
                {"double", new CommonColumnTypeProvider() },
                {"uuid", new CommonColumnTypeProvider() },
                {"single", new CommonColumnTypeProvider() },
                {"varchar", new VarcharColumnTypeProvider() },
                {"numeric", new NumericColumnTypeProvider() },
                {"float", new FloatColumnTypeProvider() }
            };
        }

        public ColumnTypeProvider GetProvider(string dataType)
        {
            return _columnInfoDictionary[dataType];
        }
    }

    public interface IColumnInfoProviderFactory
    {
        ColumnTypeProvider GetProvider(string dataType);
    }
}