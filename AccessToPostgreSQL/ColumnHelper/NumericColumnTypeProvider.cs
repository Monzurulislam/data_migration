﻿using NpgsqlTypes;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class NumericColumnTypeProvider : ColumnTypeProvider
    {
        public override NpgsqlDbType GetNpgsqlDbType(Column column)
        {
            return NpgsqlDbType.Numeric;
        }

        public override string TypeName => "numeric";
        protected override string GetColumnTypeInfo(Column column)
        {
            return column.Precision > 0 ? $"{TypeName}({column.Precision}, {column.Scale})" : TypeName;
        }
    }
}