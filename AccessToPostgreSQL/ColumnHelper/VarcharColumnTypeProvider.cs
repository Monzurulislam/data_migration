﻿using NpgsqlTypes;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class VarcharColumnTypeProvider : ColumnTypeProvider
    {
        public override NpgsqlDbType GetNpgsqlDbType(Column column)
        {
            return NpgsqlDbType.Varchar;
        }

        public override string TypeName => "varchar";


        protected override string GetColumnTypeInfo(Column column)
        {
            return column.Length > 0 ? $"{TypeName}({column.Length})" : TypeName;
        }
    }
}