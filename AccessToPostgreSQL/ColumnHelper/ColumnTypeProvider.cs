﻿using NpgsqlTypes;
namespace AccessToPostgreSQL.ColumnHelper
{
    public abstract class ColumnTypeProvider
    {
        public abstract NpgsqlDbType GetNpgsqlDbType(Column column);
        public abstract string TypeName { get; }
        public string GetColumnType(string tableName, Column column)
        {
            if (column.PrimaryKey)
            {
                return $"{column.Name} {GetColumnTypeInfo(column)} not null constraint {tableName}_pkey primary key";
            }
            var type = string.IsNullOrEmpty(column.DefaultValue) ? $"{column.Name} {GetColumnTypeInfo(column)}" : $"{column.Name} {GetColumnTypeInfo(column)} default {column.DefaultValue}";
            return column.AllowsNull ? type : $"{type} not null";
        }

        protected abstract string GetColumnTypeInfo(Column column);
    }
}