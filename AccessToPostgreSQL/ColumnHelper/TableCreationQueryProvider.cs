﻿using System.Linq;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class TableCreationQueryProvider : ITableCreationQueryProvider
    {
        private readonly string _postrgresSchema;

        public TableCreationQueryProvider(IColumnInfoProviderFactory columnInfoProviderFactory, string postrgresSchema)
        {
            ColumnInfoProviderFactory = columnInfoProviderFactory;
            _postrgresSchema = postrgresSchema;
        }

        public string GetQuery(Table table)
        {
            var tableStructure = $"create table if not exists {_postrgresSchema}.{table.Name} (";
            var columnInfoList = table.Columns.Select(c=> GetColumnInfo(table.Name, c)).ToList();
            var columnsInfo = string.Join(", ", columnInfoList.Select(c => c));
            tableStructure += columnsInfo;
            tableStructure += ")";
            return tableStructure;
        }

        public IColumnInfoProviderFactory ColumnInfoProviderFactory { get; set; }

        private string GetColumnInfo(string tableName, Column tableColumn)
        {
            return ColumnInfoProviderFactory.GetProvider(tableColumn.DataType.ToLower()).GetColumnType(tableName, tableColumn);
        }
    }

    public interface ITableCreationQueryProvider
    {
        string GetQuery(Table table);
        IColumnInfoProviderFactory ColumnInfoProviderFactory { get; set; }
    }
}