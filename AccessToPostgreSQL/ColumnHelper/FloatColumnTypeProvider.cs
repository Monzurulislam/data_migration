﻿using NpgsqlTypes;

namespace AccessToPostgreSQL.ColumnHelper
{
    public class FloatColumnTypeProvider : ColumnTypeProvider
    {
        public override NpgsqlDbType GetNpgsqlDbType(Column column)
        {
            return column.Precision >= 1 && column.Precision <= 24 ? NpgsqlDbType.Real : NpgsqlDbType.Double;
        }

        public override string TypeName => "float";
        protected override string GetColumnTypeInfo(Column column)
        {
            return column.Precision > 0 ? $"{TypeName}({column.Precision})" : "double precision";
        }
    }
}