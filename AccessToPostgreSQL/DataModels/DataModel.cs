﻿using System;
using System.Xml.Serialization;

namespace AccessToPostgreSQL
{
    [Serializable()]
    [XmlRoot("DataModel")]
    public class DataModel
    {
        public Table[] Tables { get; set; }
        public StoreProcedure[] StoreProcedures { get; set; }
    }

    [Serializable()]
    public class Table
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Alias")]
        public string Alias { get; set; }
        public Column[] Columns { get; set; }
    }

    [Serializable()]
    public class Column
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("DataType")]
        public string DataType { get; set; }
        [XmlAttribute("Precision")]
        public int Precision { get; set; }
        [XmlAttribute("AllowsNull")]
        public bool AllowsNull { get; set; }
        [XmlAttribute("Length")]
        public int Length { get; set; }
        [XmlAttribute("Scale")]
        public int Scale { get; set; }
        [XmlAttribute("PrimaryKey")]
        public bool PrimaryKey { get; set; }
        [XmlAttribute("Alias")]
        public string Alias { get; set; }
        [XmlAttribute("Description")]
        public string Description { get; set; }
        [XmlAttribute("DefaultValue")]
        public string DefaultValue { get; set; }
    }

    [Serializable()]
    public class StoreProcedure
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Schema")]
        public string Schema { get; set; }
        [XmlAttribute("Query")]
        public string Query { get; set; }
    }
}