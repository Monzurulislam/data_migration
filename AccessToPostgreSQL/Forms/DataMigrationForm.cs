﻿using System;
using System.Windows.Forms;
using AccessToPostgreSQL.Presenter;
using DevExpress.Skins;
using DevExpress.XtraEditors;

namespace AccessToPostgreSQL.Forms
{
    public partial class DataMigrationForm : XtraForm, IDataMigration
    {
        public DataMigrationForm()
        {
            SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
            InitializeComponent();
        }

        void IDataMigration.Show()
        {
            this.ShowDialog();
        }

        public event Action MigrationButtonClicked;

        private void btn_Click(object sender, EventArgs e)
        {
            MigrationButtonClicked?.Invoke();
        }

        public string AccessDataSource => txtAccessDatabase.Text;

        public void UpdateToolTip(string toolTip)
        {
            toolStripLabel1.Text = toolTip;
            Application.DoEvents();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = @"Mirosoft access files (*.accdb,*.mdb)|*.accdb;*.mdb", Multiselect = false
            };
            var result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtAccessDatabase.Text = openFileDialog.FileName;
            }
        }
    }
}
