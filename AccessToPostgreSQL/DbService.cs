﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using Npgsql;
using NpgsqlTypes;

namespace AccessToPostgreSQL
{
    public class DbService : IDbService
    {
        private readonly DbConnectionFactory _dbConnectionFactory;
        private readonly string _postgresSchema;

        public DbService(DbConnectionFactory dbConnectionFactory, string postgresSchema)
        {
            _dbConnectionFactory = dbConnectionFactory;
            _postgresSchema = postgresSchema;
        }

        public void InsertIntoPostgresTable(string accessDataSource, string tableName, List<NpgsqlDbType> columnTypes)
        {
            using (var npgsqlConnection = _dbConnectionFactory.GetNpgsqlConnection())
            {
                npgsqlConnection.Open();
                TruncateTable(tableName, npgsqlConnection);
                using (var oleDbConnection = _dbConnectionFactory.GetOleDbConnection(accessDataSource))
                {
                    oleDbConnection.Open();
                    using (var oleDbCommand = new OleDbCommand { Connection = oleDbConnection, CommandText = $"SELECT * FROM {tableName}" })
                    {
                        Insert(tableName, oleDbCommand, npgsqlConnection, columnTypes);
                    }
                }
            }
        }

        private void Insert(string tableName, OleDbCommand oleDbCommand, NpgsqlConnection npgsqlConnection, List<NpgsqlDbType> columnTypes)
        {
            try
            {
                DoInsertion(tableName, oleDbCommand, npgsqlConnection, columnTypes);
            }
            catch (Exception exception)
            {
                NLogger.Trace(exception);
            }
        }

        private void DoInsertion(string tableName, OleDbCommand oleDbCommand, NpgsqlConnection npgsqlConnection, List<NpgsqlDbType> columnTypes)
        {
            var oleDbDataReader = oleDbCommand.ExecuteReader();
            using (var writer = npgsqlConnection.BeginBinaryImport($"copy {_postgresSchema}.{tableName} from STDIN (FORMAT BINARY)"))
            {
                while (oleDbDataReader.Read())
                {
                    writer.StartRow();
                    for (var i = 0; i < oleDbDataReader.FieldCount; i++)
                    {
                        writer.Write(oleDbDataReader[i], columnTypes[i]);
                    }
                }
                writer.Complete();
                InsertionCountChanged?.Invoke(GetCount(tableName), tableName);
            }
        }

        private void TruncateTable(string tableName, NpgsqlConnection npgsqlConnection)
        {
            var query = $"TRUNCATE TABLE {_postgresSchema}.{tableName}";
            ExecuteNonQuery(npgsqlConnection, query);
        }

        public void DoMigration(string procedureName)
        {
            ExecuteNonQuery($"select {_postgresSchema}.{procedureName}()", 0);
        }

        public event Action<int, string> InsertionCountChanged;
        public void CreateSchema()
        {
            ExecuteNonQuery($"create schema if not exists {_postgresSchema}");
        }

        private void ExecuteNonQuery(NpgsqlConnection npgsqlConnection, string query, int timeOut = 30)
        {
            using (var cmdCommand = new NpgsqlCommand(query, npgsqlConnection) {CommandTimeout = timeOut})
            {
                cmdCommand.ExecuteNonQuery();
            }
        }

        private int GetCount(string tableName)
        {
            using (var npgsqlConnection = _dbConnectionFactory.GetNpgsqlConnection())
            {
                npgsqlConnection.Open();
                using (var cmdCommand = new NpgsqlCommand($"select count(*) from {_postgresSchema}.{tableName}",
                    npgsqlConnection))
                {
                    return Convert.ToInt32(cmdCommand.ExecuteScalar());
                }
            }
        }

        public void ExecuteNonQuery(string query, int timeOut = 30)
        {
            using (var npgsqlConnection = _dbConnectionFactory.GetNpgsqlConnection())
            {
                npgsqlConnection.Open();
                ExecuteNonQuery(npgsqlConnection, query, timeOut);
            }
        }
    }

    public interface IDbService
    {
        void InsertIntoPostgresTable(string accessDataSource, string tableName, List<NpgsqlDbType> columnTypes);
        void DoMigration(string procedureName);
        event Action<int, string> InsertionCountChanged;
        void CreateSchema();
        void ExecuteNonQuery(string query, int timeOut = 30);
    }
}