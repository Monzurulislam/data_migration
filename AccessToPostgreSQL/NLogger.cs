﻿using System;
using NLog;

namespace AccessToPostgreSQL
{
    public static class NLogger
    {
        public static NLog.Logger Logger
        {
            get { return LogManager.GetCurrentClassLogger(); }
        }

        public static void Info(string message)
        {
            Logger.Info(message);
        }

        public static void Error(string message)
        {
            Logger.Error(message);
        }

        public static void Error(Exception exception)
        {
            Logger.Error(exception);
        }

        public static void Trace(Exception exception)
        {
            Logger.Trace(exception);
        }

        public static void Log(LogLevel logLevel, string message)
        {
            Logger.Log(logLevel, message);
        }
    }
}