﻿using System;
using System.Configuration;
using System.Windows.Forms;
using AccessToPostgreSQL.ColumnHelper;
using AccessToPostgreSQL.Forms;
using AccessToPostgreSQL.Presenter;

namespace AccessToPostgreSQL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var connectionString = ConfigurationManager.ConnectionStrings["DbPostgresqlConnection"].ConnectionString;
            var postrgresSchema = ConfigurationManager.AppSettings["PostgresSchema"];
            var accessConnectionStringPrefix = ConfigurationManager.AppSettings["AccessConnectionStringPrefix"];
            var tableCreationQueryProvider = new TableCreationQueryProvider(new ColumnInfoProviderFactory(), postrgresSchema);
            var dbService = new DbService(new DbConnectionFactory(connectionString, accessConnectionStringPrefix), postrgresSchema);
            var dataMigrationPresenter = new DataMigrationPresenter(new DataMigrationForm(), dbService, tableCreationQueryProvider);
            dataMigrationPresenter.Show();
        }
    }
}
